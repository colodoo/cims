import request from '@/utils/request'

// 待办任务
export function getApplyingTasks(query) {
  return request({
    url: '/rest/task/get-applying-tasks',
    method: 'get',
    baseURL: '/cims',
    params: query
  })
}

// 已办任务
export function getApplyedTasks(query) {
  return request({
    url: '/rest/task/get-applyed-tasks',
    method: 'get',
    baseURL: '/cims',
    params: query
  })
}

// 我发起的流程
export function getMyProcessInstances(query) {
  return request({
    url: '/rest/task/my-processInstances',
    method: 'get',
    baseURL: '/cims',
    params: query
  })
}

export function findFormInfo(query) {
  return request({
    url: '/rest/task/find-formInfo',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

export function formInfoByInstanceId(query) {
  return request({
    url: '/rest/processInstance/find-formInfoByInstanceId',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

export function complete(query) {
  return request({
    url: '/rest/formdetail/complete/',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

export function claimTask(query) {
  return request({
    url: '/rest/formdetail/claimTask',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

export function unClaimTask(query) {
  return request({
    url: '/rest/formdetail/unClaimTask',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

export function revokeProcess(query) {
  return request({
    url: '/rest/formdetail/revokeProcess',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

export function turnTask(query) {
  return request({
    url: '/rest/formdetail/turnTask',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

export function delegateTask(query) {
  return request({
    url: '/rest/formdetail/delegateTask',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

// 前加签
export function beforeAddSignTask(query) {
  return request({
    url: '/rest/formdetail/beforeAddSignTask',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

// 后加签
export function afterAddSignTask(query) {
  return request({
    url: '/rest/formdetail/afterAddSignTask',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

// 驳回
export function doBackStep(query) {
  return request({
    url: '/rest/formdetail/doBackStep',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}

// 审批意见列表
export function commentsByProcessInstanceId(query) {
  return request({
    url: '/rest/formdetail/commentsByProcessInstanceId',
    method: 'get',
    baseURL: '/cims',
    params: query
  })
}

// 可驳回节点获取
export function getBackNodesByProcessInstanceId(query) {
  const url = '/rest/formdetail/getBackNodesByProcessInstanceId/' + query.processInstanceId + '/' + query.taskId
  return request({
    url: url,
    method: 'get',
    baseURL: '/cims'
  })
}
