import request from '@/utils/request'
import defaultSettings from '@/settings'

export function login(data) {
  if (defaultSettings.userModule) {
    return request({
      // url: '/vue-element-admin/user/login',
      url: '/app/authentication',
      method: 'post',
      baseURL: '/cims',
      params: data
    })
  } else {
    return request({
      url: '/vue-element-admin/user/login',
      method: 'post',
      params: data
    })
  }
}

export function getInfo(token) {
  return request({
    url: '/vue-element-admin/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  if (defaultSettings.userModule) {
    return request({
      // url: '/vue-element-admin/user/logout',
      url: '/app/logout',
      baseURL: '/cims',
      method: 'get'
    })
  } else {
    return request({
      url: '/vue-element-admin/user/logout',
      method: 'post'
    })
  }
}
