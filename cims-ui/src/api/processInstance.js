import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/rest/processInstance/page-model',
    method: 'post',
    baseURL: '/cims',
    params: query
  })
}

export function stopProcess(query) {
  return request({
    url: '/rest/processInstance/stopProcess',
    method: 'post',
    baseURL: '/cims',
    params: query
  })
}

export function saProcessInstanceById(query) {
  return request({
    url: '/rest/processInstance/saProcessInstanceById',
    method: 'post',
    baseURL: '/cims',
    params: query
  })
}

export function deleteProcessInstanceById(id) {
  return request({
    url: '/rest/processInstance/deleteProcessInstanceById/' + id,
    method: 'get',
    baseURL: '/cims'
  })
}
