import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/rest/model/page-model',
    method: 'get',
    baseURL: '/cims',
    params: query
  })
}

export function fetchPage() {
  return request({
    url: '',
    method: 'get',
    baseURL: '/cims'
  })
}

export function deploy(query) {
  return request({
    url: '/rest/model/deploy',
    method: 'post',
    baseURL: '/cims',
    params: query
  })
}

export function loadPngByModelId(modelId) {
  return request({
    url: '/rest/model/loadPngByModelId/' + modelId,
    method: 'get',
    baseURL: '/cims'
  })
}

export function loadXmlByModelId(modelId) {
  return request({
    url: '/rest/model/loadXmlByModelId/' + modelId,
    method: 'get',
    baseURL: '/cims'
  })
}
