import request from '@/utils/request'

// 分页定义管理
export function fetchList(query) {
  return request({
    url: '/rest/definition/page-model',
    method: 'post',
    baseURL: '/cims',
    params: query
  })
}

// 定义管理 XML
export function processFileXml(id) {
  return request({
    url: '/rest/definition/processFile/xml/' + id,
    method: 'get',
    baseURL: '/cims'
  })
}

// 定义管理 流程图
export function processFileImg(id) {
  return request({
    url: '/rest/definition/processFile/img/' + id + '?t=' + Date.parse(new Date()),
    method: 'get',
    baseURL: '/cims'
  })
}

/**
 * 定义管理 挂起（激活）
 * id ID
 * suspensionState 状态 1-挂起 2-激活
 * @param {*} query
 */
export function saDefinitionById(query) {
  return request({
    url: '/rest/definition/saDefinitionById',
    method: 'post',
    baseURL: '/cims',
    params: query
  })
}

/**
 * 定义管理 删除
 * deploymentId ID
 * @param {*} query
 */
export function deleteDeployment(query) {
  return request({
    url: '/rest/definition/deleteDeployment',
    method: 'post',
    baseURL: '/cims',
    params: query
  })
}
