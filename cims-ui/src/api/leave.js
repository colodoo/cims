import request from '@/utils/request'

export function fetchList(query) {
  query.sortOrder = 'start_time desc'
  return request({
    url: '/rest/leave/list',
    method: 'get',
    baseURL: '/cims',
    params: query
  })
}

export function add(query) {
  return request({
    url: '/rest/leave/add/',
    method: 'post',
    baseURL: '/cims',
    data: query
  })
}
