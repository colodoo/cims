/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const InstanceRouter = {
  path: '/instance',
  component: Layout,
  redirect: '/instance/leave',
  name: 'Table',
  meta: {
    title: '请假管理',
    icon: 'table'
  },
  children: [
    {
      path: 'leave',
      component: () => import('@/views/instance/leave'),
      name: 'InstanceLeave',
      meta: { title: '请假管理' }
    }
  ]
}
export default InstanceRouter
