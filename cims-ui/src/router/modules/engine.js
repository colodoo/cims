/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const EngineRouter = {
  path: '/engine',
  component: Layout,
  redirect: '/engine/model',
  name: 'Table',
  meta: {
    title: 'engine',
    icon: 'table'
  },
  children: [
    {
      path: 'model',
      component: () => import('@/views/engine/model'),
      name: 'EngineModel',
      meta: { title: '模板管理' }
    },
    {
      path: 'model-new',
      component: () => import('@/views/engine/modelNew'),
      name: 'EngineModelNew',
      meta: { title: '模板维护' },
      hidden: true
    },
    {
      path: 'defintion',
      component: () => import('@/views/engine/definition'),
      name: 'EngineDefintion',
      meta: { title: '定义管理' }
    },
    {
      path: 'processInstance',
      component: () => import('@/views/engine/processInstance'),
      name: 'EngineProcessInstance',
      meta: { title: '实例管理' }
    },
    {
      path: 'tasks',
      component: () => import('@/views/engine/task'),
      name: 'EngineTasks',
      meta: { title: '任务管理' }
    }
  ]
}
export default EngineRouter
