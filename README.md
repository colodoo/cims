# cims

#### 介绍
1.整合官方flowable modeler 进行流程设计、表单设计
2.提供flowable rest api 接口 供后台业务系统流程调用
3.提供前后端分离后台管理进行模拟流程测试、流程管理


#### 软件架构
软件架构说明
后端项目cims-manager：JDK8+SprigBoot+flowable
前端cims-ui：Vue+ElementUI+Vue-Element-Admin
文档说明：doc



#### 安装教程

1.  搭建JDK+maven等基础开发环境，运行doc/libs/install.bat，将依赖包添加到本地仓库
2.  运行运行doc/libs/install.bat，将依赖包添加到本地仓库，将依赖包添加到本地仓库
3.  后端启动类  com.cims.bpm.SpringBootApplication
4.  前台启动 cims-ui  目录下运行  npm run dev,前端访问地址：http://locahost:9527/

#### 使用说明
1.  项目适用于作为流程业务系统提供流程服务
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 交流反馈
    欢迎加入QQ群讨论：925878709
