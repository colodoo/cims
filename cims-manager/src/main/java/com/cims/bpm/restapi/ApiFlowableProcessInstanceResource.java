package com.cims.bpm.restapi;

import com.cims.bpm.vo.FormInfoQueryVo;
import com.cims.bpm.vo.ProcessInstanceQueryVo;
import com.dragon.tools.common.ReturnCode;
import com.dragon.tools.pager.PagerModel;
import com.dragon.tools.pager.Query;
import com.dragon.tools.vo.ReturnVo;
import com.cims.bpm.service.IFlowableProcessInstanceService;
import com.cims.bpm.vo.EndProcessVo;
import com.cims.bpm.vo.ProcessInstanceVo;
import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.form.api.FormInfo;
import org.flowable.form.model.FormField;
import org.flowable.form.model.SimpleFormModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/processInstance")
public class ApiFlowableProcessInstanceResource extends BaseResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiFlowableProcessInstanceResource.class);
    @Autowired
    private IFlowableProcessInstanceService flowableProcessInstanceService;


    @Autowired
    private HistoryService historyService;

    /**
     * 分页查询流程定义列表
     *
     * @param params 参数
     * @param query  分页
     * @return
     */
    @PostMapping(value = "/page-model")
    public PagerModel<ProcessInstanceVo> pageModel(ProcessInstanceQueryVo params, Query query) {
        PagerModel<ProcessInstanceVo> pm = flowableProcessInstanceService.getPagerModel(params, query);
        return pm;
    }

    /**
     * 删除流程实例haha
     *
     * @param processInstanceId 参数
     * @return
     */
    @GetMapping(value = "/deleteProcessInstanceById/{processInstanceId}")
    public ReturnVo<String> deleteProcessInstanceById(@PathVariable String processInstanceId) {
        ReturnVo<String> data = flowableProcessInstanceService.deleteProcessInstanceById(processInstanceId);
        return data;
    }

    /**
     * 激活或者挂起流程定义
     *
     * @param id
     * @return
     */
    @PostMapping(value = "/saProcessInstanceById")
    public ReturnVo<String> saProcessInstanceById(String id, int suspensionState) {
        ReturnVo<String> returnVo = flowableProcessInstanceService.suspendOrActivateProcessInstanceById(id, suspensionState);
        return returnVo;
    }

    /**
     * 终止
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/stopProcess")
    public ReturnVo<String> stopProcess(EndProcessVo params) {
        boolean flag = this.isSuspended(params.getProcessInstanceId());
        ReturnVo<String> returnVo = null;
        if (flag){
            params.setMessage("后台执行终止");
            params.setUserCode(this.getLoginUser().getId());
            returnVo = flowableProcessInstanceService.stopProcessInstanceById(params);
        }else{
            returnVo = new ReturnVo<>(ReturnCode.FAIL,"流程已挂起，请联系管理员激活!");
        }
        return returnVo;
    }



    //add by huxipi
    @PostMapping(value = "/find-formInfoByInstanceId")
    public ReturnVo<FormField> findFormInfoByByTaskId(FormInfoQueryVo params) throws Exception {
        ReturnVo<FormField> returnVo = new ReturnVo<>(ReturnCode.SUCCESS, "OK");
        HistoricProcessInstance processInstance= historyService.createHistoricProcessInstanceQuery().
                processInstanceId(params.getProcessInstanceId()).singleResult();
        String processDefinitionId=processInstance.getProcessDefinitionId();

        //查询表单信息
        FormInfo fm = runtimeService.getStartFormModel(processDefinitionId, params.getProcessInstanceId());
        List<FormField> fields = ((SimpleFormModel) fm.getFormModel()).getFields();
        returnVo.setDatas(fields);
        return returnVo;
    }

}