package com.cims.bpm.restapi;

import com.cims.bpm.vo.*;
import com.dragon.tools.common.ReturnCode;
import com.dragon.tools.pager.PagerModel;
import com.dragon.tools.pager.Query;
import com.dragon.tools.vo.ReturnVo;
import com.cims.bpm.service.IFlowableProcessInstanceService;
import com.cims.bpm.service.IFlowableTaskService;
import com.cims.bpm.service.impl.leave.ILeaveService;
import com.cims.bpm.service.impl.leave.Leave;
import com.cims.bpm.vo.*;
import org.flowable.engine.FormService;
import org.flowable.engine.HistoryService;
import org.flowable.engine.form.FormProperty;
import org.flowable.engine.form.TaskFormData;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/task")
public class ApiFlowableTaskResource extends BaseResource {

    @Autowired
    private IFlowableTaskService flowableTaskService;
    @Autowired
    private IFlowableProcessInstanceService flowableProcessInstanceService;
    @Autowired
    private ILeaveService leaveService;
    @Autowired
    private HistoryService historyService;

    @Autowired
    private FormService formService;

    /**
     * 获取待办任务列表
     *
     * @param params 参数
     * @param query  查询条件
     * @return
     */
    @GetMapping(value = "/get-applying-tasks")
    public PagerModel<TaskVo> getApplyingTasks(TaskQueryVo params, Query query) {
      //  params.setUserCode(this.getLoginUser().getId());
        PagerModel<TaskVo> pm = flowableTaskService.getApplyingTasks(params, query);
        return pm;
    }

    /**
     * 获取已办任务列表
     *
     * @param params 参数
     * @param query  查询条件
     * @return
     */
    @GetMapping(value = "/get-applyed-tasks")
    public PagerModel<TaskVo> getApplyedTasks(TaskQueryVo params, Query query) {
       // params.setUserCode(this.getLoginUser().getId());
        PagerModel<TaskVo> pm = flowableTaskService.getApplyedTasks(params, query);
        return pm;
    }

    /**
     * 获取我发起的流程
     *
     * @param params 参数
     * @param query  查询条件
     * @return
     */
    @GetMapping(value = "/my-processInstances")
    public PagerModel<ProcessInstanceVo> myProcessInstances(ProcessInstanceQueryVo params, Query query) {
        //params.setUserCode(this.getLoginUser().getId());
        PagerModel<ProcessInstanceVo> pm = flowableProcessInstanceService.getMyProcessInstances(params, query);
        return pm;
    }



    /**
     * 查询表单详情
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/find-formInfo")
    public ReturnVo<FormInfoVo> findFormInfoByFormInfoQueryVo(@RequestBody FormInfoQueryVo params) throws Exception{
        ReturnVo<FormInfoVo> returnVo = new ReturnVo<>(ReturnCode.SUCCESS,"OK");
        FormInfoVo<Leave> formInfoVo = new FormInfoVo(params.getTaskId(),params.getProcessInstanceId());
        String processInstanceId = params.getProcessInstanceId();
        String businessKey = null;
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        if (processInstance == null){
            HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
            businessKey = historicProcessInstance.getBusinessKey();
        }else {
            businessKey = processInstance.getBusinessKey();
        }

        if(null==params.getBusinessName()){
            formInfoVo.setBusinessKey(businessKey);
        }else{
            if("leave".equals(params.getBusinessName())){
                Leave leave = leaveService.getLeaveById(businessKey);
                formInfoVo.setFormInfo(leave);
            }
        }
        returnVo.setData(formInfoVo);
        return returnVo;
    }




    //add by huxipi
    @PostMapping(value = "/find-formInfoByTaskId")
    public ReturnVo<FormProperty> findFormInfoByByTaskId(FormInfoQueryVo params) throws Exception{
        ReturnVo<FormProperty> returnVo = new ReturnVo<>(ReturnCode.SUCCESS,"OK");
        //查询表单信息
        TaskFormData taskFormData=formService.getTaskFormData(params.getTaskId());
         returnVo.setDatas(taskFormData.getFormProperties());
        return returnVo;

    }













}