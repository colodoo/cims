package com.cims.bpm.restapi;

import com.cims.bpm.service.IFlowableCommentService;
import com.cims.bpm.vo.*;
import com.dragon.tools.common.ReturnCode;
import com.dragon.tools.vo.ReturnVo;
import com.cims.bpm.service.IFlowableProcessInstanceService;
import com.cims.bpm.service.IFlowableTaskService;
import com.cims.bpm.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/rest/formdetail")
public class    ApiFormDetailReource extends BaseResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiFormDetailReource.class);
    @Autowired
    private IFlowableCommentService flowableCommentService;
    @Autowired
    private IFlowableTaskService flowableTaskService;
    @Autowired
    private IFlowableProcessInstanceService flowableProcessInstanceService;

    /**
     * 通过流程实例id获取审批意见
     *
     * @param processInstanceId 流程实例id
     * @return
     */
    @GetMapping("/commentsByProcessInstanceId")
    public List<CommentVo> commentsByProcessInstanceId(String processInstanceId) {
        List<CommentVo> datas = flowableCommentService.getFlowCommentVosByProcessInstanceId(processInstanceId);
        return datas;
    }

    /**
     * 审批任务
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/complete")
    public ReturnVo<String> complete(@RequestBody CompleteTaskVo params) {
        boolean flag = this.isSuspended(params.getProcessInstanceId());
        ReturnVo<String> returnVo = null;
        if (flag){
            params.setUserCode(params.getUserCode());
            returnVo = flowableTaskService.complete(params);
        }else{
            returnVo = new ReturnVo<>(ReturnCode.FAIL,"流程已挂起，请联系管理员激活!");
        }
        return returnVo;
    }

    /**
     * 终止
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/stopProcess")
    public ReturnVo<String> stopProcess(EndProcessVo params) {
        boolean flag = this.isSuspended(params.getProcessInstanceId());
        ReturnVo<String> returnVo = null;
        if (flag){
            params.setUserCode(this.getLoginUser().getId());
            returnVo = flowableProcessInstanceService.stopProcessInstanceById(params);
        }else{
            returnVo = new ReturnVo<>(ReturnCode.FAIL,"流程已挂起，请联系管理员激活!");
        }
        return returnVo;
    }

    /**
     * 撤回
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/revokeProcess")
    public ReturnVo<String> revokeProcess(@RequestBody RevokeProcessVo params) {
        //params.setUserCode(this.getLoginUser().getId());
        ReturnVo<String> returnVo = flowableProcessInstanceService.revokeProcess(params);
        return returnVo;
    }
    /**
     * 转办
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/turnTask")
    public ReturnVo<String> turnTask(@RequestBody TurnTaskVo params) {
        ReturnVo<String> returnVo = null;

        if (params.getUserCodes() != null && params.getUserCodes().length > 0) {
            //params.setUserCode(this.getLoginUser().getId());
            params.setTurnToUserId("pm");//把pm的任务交给mp转办
            params.setTurnToUserId(params.getUserCodes()[0]);
            returnVo = flowableTaskService.turnTask(params);
        }else {
            returnVo = new ReturnVo<>(ReturnCode.FAIL,"请选择人员");
        }
        return returnVo;
    }

    /**
     * 委派
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/delegateTask")
    public ReturnVo<String> delegateTask(@RequestBody DelegateTaskVo params) {
        ReturnVo<String> returnVo = null;
        if (params.getUserCodes()!= null && params.getUserCodes().length > 0) {
            params.setUserCode(params.getUserCode());
            params.setDelegateUserCode(params.getUserCodes()[0]);
            returnVo = flowableTaskService.delegateTask(params);
        }else {
            returnVo = new ReturnVo<>(ReturnCode.FAIL,"请选择人员");
        }
        return returnVo;
    }

    /**
     * 签收
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/claimTask")
    public ReturnVo<String> claimTask(@RequestBody ClaimTaskVo params) {
        //params.setUserCode(this.getLoginUser().getId());
        ReturnVo<String> returnVo = flowableTaskService.claimTask(params);
        return returnVo;
    }

    /**
     * 反签收
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/unClaimTask")
    public ReturnVo<String> unClaimTask(@RequestBody ClaimTaskVo params) {
        //params.setUserCode(this.getLoginUser().getId());
        ReturnVo<String> returnVo = flowableTaskService.unClaimTask(params);
        return returnVo;
    }

    /**
     * 向前加签
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/beforeAddSignTask")
    public ReturnVo<String> beforeAddSignTask(@RequestBody AddSignTaskVo params) {
        ReturnVo<String> returnVo = null;
        if (params.getUserCodes() != null && params.getUserCodes().length > 0) {
           // params.setUserCode(this.getLoginUser().getId());
            params.setSignPersoneds(Arrays.asList(params.getUserCodes()));
            returnVo = flowableTaskService.beforeAddSignTask(params);
        }else {
            returnVo = new ReturnVo<>(ReturnCode.FAIL,"请选择人员");
        }
        return returnVo;
    }

    /**
     * 向后加签
     *
     * @param params 参数
     * @return
     */
    @PostMapping(value = "/afterAddSignTask")
    public ReturnVo<String> afterAddSignTask(@RequestBody AddSignTaskVo params) {
        ReturnVo<String> returnVo = null;
        if (params.getUserCodes() != null && params.getUserCodes().length > 0) {
        //    params.setUserCode(this.getLoginUser().getId());
            params.setSignPersoneds(Arrays.asList(params.getUserCodes()));
            returnVo = flowableTaskService.afterAddSignTask(params);
        }else {
            returnVo = new ReturnVo<>(ReturnCode.FAIL,"请选择人员");
        }
        return returnVo;
    }



    /**
     * 获取实例图片
     * @param processInstanceId 流程实例id
     * @return
     */
    @GetMapping(value = "/image/{processInstanceId}/{imageType}")
    public void image(@PathVariable String processInstanceId, HttpServletResponse response,@PathVariable String imageType) {
        try {
            byte[] b = flowableProcessInstanceService.createImage(processInstanceId,imageType);
           // response.setHeader("Content-type", "image/png;charset=UTF-8");
           // response.setHeader("Content-type", "image/gif;charset=UTF-8");
            response.setHeader("Content-type","image/"+imageType+";charset=UTF-8");
            response.getOutputStream().write(b);
        } catch (Exception e) {
            LOGGER.error("ApiFormDetailReource-image:" + e);
            e.printStackTrace();
        }
    }

    /**
     * 获取可驳回节点列表
     * @param processInstanceId 流程实例id
     * @return
     */
    @GetMapping(value = "/getBackNodesByProcessInstanceId/{processInstanceId}/{taskId}")
    public ReturnVo<FlowNodeVo> getBackNodesByProcessInstanceId(@PathVariable String processInstanceId,@PathVariable String taskId) {
        List<FlowNodeVo> datas = flowableTaskService.getBackNodesByProcessInstanceId(processInstanceId,taskId);
        ReturnVo<FlowNodeVo> returnVo = new ReturnVo<>(ReturnCode.SUCCESS,"查询返回节点成功");
        returnVo.setDatas(datas);
        return returnVo;

    }

    /**
     * 驳回节点
      * @param params 参数
     * @return
     */
    @PostMapping(value = "/doBackStep")
    public ReturnVo<String> doBackStep(@RequestBody BackTaskVo params) {
       // params.setUserCode(this.getLoginUser().getId());
        ReturnVo<String> returnVo = flowableTaskService.backToStepTask(params);
        return returnVo;
    }
}