package com.cims.bpm.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class FormInfoQueryVo implements Serializable {

    /**
     * 任务id
     */
    private String taskId;
    /**
     * 流程实例id
     */
    private String processInstanceId;


    /**
     * 表单id
     */
    private String businessKey;


    /**
     * 表单Name
     */
    private String businessName;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }
}
