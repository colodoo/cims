package com.cims.bpm.vo;

import lombok.Data;

import java.util.List;

@Data
public class AddSignTaskVo extends BaseProcessVo {
    /**
     * 被加签人
     */
    private List<String> signPersoneds;

    public List<String> getSignPersoneds() {
        return signPersoneds;
    }

    public void setSignPersoneds(List<String> signPersoneds) {
        this.signPersoneds = signPersoneds;
    }


    public String[] userCodes;
}
