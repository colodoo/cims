package com.cims.bpm.vo;

import java.io.Serializable;

public class TaskQueryVo implements Serializable {

    /**
     * 用户工号
     */
    private String userCode;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
}
