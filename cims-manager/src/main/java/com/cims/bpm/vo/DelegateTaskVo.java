package com.cims.bpm.vo;

import lombok.Data;

@Data
public class DelegateTaskVo extends BaseProcessVo {
    /**
     * 委派人
     */
    private String delegateUserCode;

    public String[] userCodes;

    public String getDelegateUserCode() {
        return delegateUserCode;
    }

    public void setDelegateUserCode(String delegateUserCode) {
        this.delegateUserCode = delegateUserCode;
    }
}
