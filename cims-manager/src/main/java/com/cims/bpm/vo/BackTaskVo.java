package com.cims.bpm.vo;

public class BackTaskVo extends BaseProcessVo {

    /**
     * 需要驳回的节点id 必填
     */
    private String distFlowElementId;

    public String getDistFlowElementId() {
        return distFlowElementId;
    }

    public void setDistFlowElementId(String distFlowElementId) {
        this.distFlowElementId = distFlowElementId;
    }
}
