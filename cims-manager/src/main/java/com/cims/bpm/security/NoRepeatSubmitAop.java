package com.cims.bpm.security;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.flowable.idm.api.User;
import org.flowable.ui.common.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.common.cache.Cache;

@Aspect
@Component
@Slf4j
public class NoRepeatSubmitAop {



    @Autowired
    private Cache<String, Integer> cache;



    @Pointcut("execution(public * ApiFlowableGroupResource.*(..))")
   public void repeatSubmit(){}



   // @Around("execution(public * ApiFlowableGroupResource.*(..)) && @annotation(NoRepeatSubmit)")
   // public Object arround(ProceedingJoinPoint joinPoint, NoRepeatSubmit nrs) {
    @Before("repeatSubmit()")
    public Object interceptor(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            //获取当前用户token，识别用户
            User user = SecurityUtils.getCurrentUserObject();
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            //String sessionId = RequestContextHolder.getRequestAttributes().getSessionId();
            String sessionId =user.getId();
            HttpServletRequest request = attributes.getRequest();

            String key = sessionId + "-" + request.getServletPath();
            if (cache.getIfPresent(key) == null) {// 如果缓存中有这个url视为重复提交
                Object o = joinPoint.proceed();
                cache.put(key, 0);
                return o;
            } else {
                log.error("重复提交");
                return null;
            }
        } catch (Throwable e) {
            e.printStackTrace();
            log.error("验证重复提交时出现未知异常!");
            return "{\"code\":-889,\"message\":\"验证重复提交时出现未知异常!\"}";
        }


    }

}