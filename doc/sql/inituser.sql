
--用户
INSERT INTO `act_id_user` VALUES ('00000001', '2', '军哥', '军哥', null, '军哥@qq.com', '$2a$10$5DPO5NwLz1FPLt/3PczT0O6Z4rFE0ymq6XVh70UcDb4MzdW9AX5vi', null, null);
INSERT INTO `act_id_user` VALUES ('00000002', '2', '张三', '张三', null, '张三@qq.com', '$2a$10$W2KsNrZV9cdMPLTD1O5AnOiNtRciSzoRbe1gS.rKykjwVofR0vZ3q', null, null);
INSERT INTO `act_id_user` VALUES ('00000003', '2', '李四', '李四', null, '李四@qq.com', '$2a$10$DjuId9btXBpqEIkVXK5uEentpNYHy5qtzClTzH9EdYDCZ45oDTcXa', null, null);
INSERT INTO `act_id_user` VALUES ('00000004', '2', '王五', '王五', null, '王五@qq.com', '$2a$10$1CWSWilV3bjCkTzpStzNo.t7LvVeJ3VbrbQI6BhwGK0FjVywrGTbe', null, null);
INSERT INTO `act_id_user` VALUES ('admin', '1', 'admin', 'admin', null, null, '$2a$10$3bAePn80TtFEQGjRnpBdxeP.VUlD3pi2nyHOrYcJaT2JC8OecsrHe', null, null);

--权限
INSERT INTO `act_id_priv` VALUES ('7cb1b95005db11eab327dc8b287b3603', 'access-admin');
INSERT INTO `act_id_priv` VALUES ('7cade8be05db11eab327dc8b287b3603', 'access-idm');
INSERT INTO `act_id_priv` VALUES ('7cb4516205db11eab327dc8b287b3603', 'access-modeler');
INSERT INTO `act_id_priv` VALUES ('7cb95a7605db11eab327dc8b287b3603', 'access-rest-api');
INSERT INTO `act_id_priv` VALUES ('7cb6e97405db11eab327dc8b287b3603', 'access-task');

--用户权限
INSERT INTO `act_id_priv_mapping` VALUES ('7cb032af05db11eab327dc8b287b3603', '7cade8be05db11eab327dc8b287b3603', 'admin', null);
INSERT INTO `act_id_priv_mapping` VALUES ('7cb3670105db11eab327dc8b287b3603', '7cb1b95005db11eab327dc8b287b3603', 'admin', null);
INSERT INTO `act_id_priv_mapping` VALUES ('7cb5ff1305db11eab327dc8b287b3603', '7cb4516205db11eab327dc8b287b3603', 'admin', null);
INSERT INTO `act_id_priv_mapping` VALUES ('7cb8701505db11eab327dc8b287b3603', '7cb6e97405db11eab327dc8b287b3603', 'admin', null);
INSERT INTO `act_id_priv_mapping` VALUES ('7cba92f705db11eab327dc8b287b3603', '7cb95a7605db11eab327dc8b287b3603', 'admin', null);
INSERT INTO `act_id_priv_mapping` VALUES ('fc5eb2150db411eaa1d4dc8b287b3603', '7cade8be05db11eab327dc8b287b3603', '00000001', null);
INSERT INTO `act_id_priv_mapping` VALUES ('fc7df9e60db411eaa1d4dc8b287b3603', '7cade8be05db11eab327dc8b287b3603', '00000002', null);
INSERT INTO `act_id_priv_mapping` VALUES ('fca000d70db411eaa1d4dc8b287b3603', '7cade8be05db11eab327dc8b287b3603', '00000003', null);
INSERT INTO `act_id_priv_mapping` VALUES ('fcbf21980db411eaa1d4dc8b287b3603', '7cade8be05db11eab327dc8b287b3603', '00000004', null);




